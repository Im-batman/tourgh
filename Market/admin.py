from django.contrib import admin
from .models import market

# Register your models here.



class marketAdmin(admin.ModelAdmin):
    models= market
    max_num=2
    list_display=('name','market_type','region', 'town',)
    list_filter = ['name', 'region', 'town']
    search_fields=['name',]




admin.site.register(market, marketAdmin)