from django.shortcuts import render

# Create your views here.

from rest_framework import viewsets
from .models import market
from Data_Sorting.models import region,town,market_type
#from Restaurant.models import restaurant
from serializers import  MarketSerializer
import django_filters
from django_filters.rest_framework import DjangoFilterBackend

	
# Create your views here.
class MarketViewSet(viewsets.ModelViewSet):
 	queryset = market.objects.all()
 	serializer_class = MarketSerializer
 	filter_backends = (django_filters.rest_framework.DjangoFilterBackend,)
 	filter_fields = ('region', 'market_type','town','name')



