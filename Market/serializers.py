from rest_framework import serializers
from Data_Sorting.models import region,town,market_type
from .models import market



class RegionSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = region
		fields =('id', 'name')

class Market_typeSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = market_type
		fields =('id','mtype')


class TownSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = town
		fields = ('id','name')


class MarketSerializer(serializers.HyperlinkedModelSerializer):
	region= RegionSerializer()
	town= TownSerializer()
	market_type= Market_typeSerializer()

	class Meta:
		model = market
		fields =( 'id', 'region','town','market_type','gmap_url', 'name','image','image2','image3','description')



