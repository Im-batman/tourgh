from __future__ import unicode_literals

from django.db import models
from Data_Sorting.models import region,town,market_type

# Create your models here.

class market(models.Model):
	name=models.CharField(max_length=100, unique=True)
	market_type=models.ForeignKey(market_type, null=True)
	region=models.ForeignKey(region)
	town=models.ForeignKey(town)
	phone=models.CharField(max_length=10, blank=True)
	image=models.FileField(upload_to='market/', null=True, blank=True)
	image2=models.FileField(upload_to='market/', null=True, blank=True)
	image3=models.FileField(upload_to='market/', null=True, blank=True)
	gmap_url=models.CharField(max_length=200, null=True, blank=True)
	lon=models.DecimalField(max_digits=8, decimal_places=6)
	lat=models.DecimalField(max_digits=8, decimal_places=6)
	description=models.TextField()

	def __str__(self):
		return self.name
