from django.contrib import admin
from .models import region,rating,town,site_kind,market_type,restaurant_type
# Register your models here.

admin.site.register(region)
admin.site.register(rating)
admin.site.register(town)
admin.site.register(site_kind)
admin.site.register(market_type)
admin.site.register(restaurant_type)