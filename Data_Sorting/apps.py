from __future__ import unicode_literals

from django.apps import AppConfig


class DataSortingConfig(AppConfig):
    name = 'Data_Sorting'
    verbose_name = 'Data'
