from __future__ import unicode_literals

from django.db import models

# Create your models here.
class region(models.Model):
	name= models.CharField(max_length=100, unique=True)
	def __str__(self):
		return self.name 

class rating(models.Model):
	stars=models.CharField(max_length=5, unique=True)

	def __str__(self):
		return self.stars 


class town(models.Model):
	name=models.CharField(max_length=50, unique=True)

	def __str__(self):
		return self.name


class site_kind(models.Model):
	kind=models.CharField(max_length=50, unique=True)

	def __str__(self):
		return self.kind

class market_type(models.Model):
 	mtype=models.CharField(max_length=20, unique=True)

 	def __str__(self):
 		return self.mtype


class restaurant_type(models.Model):
	rtype=models.CharField(max_length=20, unique=True)
	
	
	def __str__(self):
 		return self.rtype
