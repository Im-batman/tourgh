from django.contrib import admin
from .models import restaurant
# Register your models here.




class restaurantAdmin(admin.ModelAdmin):
    models= restaurant
    max_num=2
    list_display=('name','region', 'town','rating',)
    list_filter = ['name', 'region']
    search_fields=['name',]




admin.site.register(restaurant, restaurantAdmin)