from __future__ import unicode_literals

from django.db import models
from Data_Sorting.models import region,rating,town,restaurant_type

# Create your models here.

class restaurant(models.Model):
	name=models.CharField(max_length=100, unique=True)
	address=models.CharField(max_length=100)
	rating=models.ForeignKey(rating)
	region=models.ForeignKey(region)
	town=models.ForeignKey(town)
	restaurant_type=models.ForeignKey(restaurant_type, blank=True, null=True)
	lon=models.DecimalField(max_digits=8, decimal_places=6, null=True)
	lat=models.DecimalField(max_digits=8, decimal_places=6, null=True)
	gmap_url=models.CharField(max_length=200, null=True)
	email=models.EmailField(null=True)
	phone=models.CharField(max_length=10, blank=True)
	image=models.FileField(upload_to='restaurant/', null=True, blank=True)
	image2=models.FileField(upload_to='restaurant/', null=True, blank=True)
	image3=models.FileField(upload_to='restaurant/', null=True, blank=True)
	website=models.URLField(blank=True)
	description=models.TextField()
	#delicacie=models.ForeignKey(menu, null=True)
	def __str__(self):
		return self.name


#class menu(models.Model):
	#name=models.CharField(max_length=50)

	#def __str__(self):
	#	return self.name