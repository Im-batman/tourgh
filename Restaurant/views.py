from django.shortcuts import render

# Create your views here.

from rest_framework import viewsets
from .models import restaurant
from Data_Sorting.models import region,rating,town,restaurant_type
#from Restaurant.models import restaurant
from serializers import  RestaurantSerializer
import django_filters
from django_filters.rest_framework import DjangoFilterBackend

	
# Create your views here.
class RestaurantViewSet(viewsets.ModelViewSet):
 	queryset = restaurant.objects.all()
 	serializer_class = RestaurantSerializer
 	filter_backends = (django_filters.rest_framework.DjangoFilterBackend,)
 	filter_fields = ('region', 'rating','town','restaurant_type','name')



