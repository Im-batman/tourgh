from rest_framework import serializers
from Data_Sorting.models import region,rating,town,restaurant_type
from .models import restaurant



class RegionSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = region
		fields =('id', 'name')

class RatingSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = rating
		fields =('id','stars')


class TownSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = town
		fields = ('id','name')

class Restaurant_typeSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model= restaurant_type
		fields= ('id','rtype')

class RestaurantSerializer(serializers.HyperlinkedModelSerializer):
	region= RegionSerializer()
	town= TownSerializer()
	rating= RatingSerializer()
	restaurant_type= Restaurant_typeSerializer()

	class Meta:
		model = restaurant
		fields =( 'id', 'region','rating','restaurant_type','town', 'name', 'address','website','gmap_url','lon','lat',
			'email','phone','image','image2','image3','description')



