"""Touring URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

from django.conf import settings
from django.conf.urls.static import static


from django.conf.urls import url, include
from django.contrib import admin
from Hotel import views as H_views
from Restaurant import views as R_views
from Tourist_Site import views as T_views
from Market import views as M_views
#from Restaurant import views as R_views
#from Data_Sorting import views
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'hotel', H_views.HotelViewSet)
router.register(r'restaurant', R_views.RestaurantViewSet)
router.register(r'site', T_views.Tourist_siteViewSet)
router.register(r'market', M_views.MarketViewSet)

'''
router.register(r'region', views.RegionViewSet)
router.register(r'rating', views.RatingViewSet)
router.register(r'town', views.TownViewSet)
'''

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^api/', include(router.urls)),
     url(r'^api/(?P<pk>[0-9]+)/', include(router.urls)),
    url(r'^', include("Hotel.urls")),


    
     
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) 

