from django.shortcuts import render, get_object_or_404
import django_filters
from rest_framework import viewsets, generics
from .models import hotel
from Data_Sorting.models import region,rating,town,site_kind
#from Restaurant.models import restaurant
from serializers import HotelSerializer,RegionSerializer,TownSerializer,RatingSerializer, Site_kindSerializer 
from django_filters.rest_framework import DjangoFilterBackend
	
# Create your views here.


class hotleFilter(django_filters.rest_framework.FilterSet):
	  region = django_filters.CharFilter(name="region__name")
	  class Meta:
	  	model = hotel
	  	fields = ['name']



class HotelViewSet(viewsets.ModelViewSet):
 	queryset = hotel.objects.all()
 	serializer_class = HotelSerializer
 	filter_backends = (django_filters.rest_framework.DjangoFilterBackend,)
 	filter_fields = ('region', 'rating','town', 'name')
 	#filter_class = hotelFilter
 	




class Hoteldetail(generics.RetrieveUpdateDestroyAPIView):
	queryset = hotel.objects.all()
 	serializer_class = HotelSerializer






class RegionViewSet(viewsets.ModelViewSet):
 	queryset = region.objects.all()
 	serializer_class =RegionSerializer

class RatingViewSet(viewsets.ModelViewSet):
	queryset = rating.objects.all()
	serializer_class = RatingSerializer


class TownViewSet(viewsets.ModelViewSet):
	queryset = town.objects.all()
	serializer_class = TownSerializer


class Site_kindViewSet(viewsets.ModelViewSet):
	queryset = site_kind.objects.all()
	serializer_class = Site_kindSerializer



def landing(request):
	context =locals()
	return render( request, "landing.html", context)


def hotel_listing(request):
 	queryset = hotel.objects.all()
 	context={
 	"hotel_list": queryset,
 	"title":"list"

 	}
 	return render( request, "hotel_listing.html", context)

def hotel_detail(request, id=None):
	instance = get_object_or_404(hotel, id=id)

	context={
	"title": instance.name,
	"instance": instance,

	}
	return render(request, "hotel_detail.html", context)
			

