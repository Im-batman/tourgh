from __future__ import unicode_literals

from django.db import models
from Data_Sorting.models import region,rating,town
# Create your models here.


class hotel(models.Model):
	name=models.CharField(max_length=100, unique=True)
	address=models.CharField(max_length=100)
	rating=models.ForeignKey(rating)
	region=models.ForeignKey(region)
	town=models.ForeignKey(town)
	dollarprice=models.DecimalField(max_digits=7, decimal_places=2, null=True, blank=True)
	cediprice=models.DecimalField(max_digits=7, decimal_places=2, null=True, blank=True)
	lon=models.DecimalField(max_digits=8, decimal_places=6, null=True, blank=True)
	lat=models.DecimalField(max_digits=8, decimal_places=6, null=True, blank=True)
	gmap_url=models.CharField(max_length=200, null=True)
	email=models.EmailField(null=True)
	phone=models.CharField(max_length=10, blank=True)
	image=models.FileField(upload_to='hotel/', null=True, blank=True)
	image2=models.FileField(upload_to='hotel/', null=True, blank=True)
	image3=models.FileField(upload_to='hotel/', null=True, blank=True)
	website=models.URLField(blank=True)
	description=models.TextField()

	def __str__(self):
		return self.name
