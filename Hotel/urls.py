from django.conf.urls import url
from django.contrib import admin
from . import views



urlpatterns = [
   
    url(r'^$', views.landing, name= 'landing'),
    url(r'^hotel/', views.hotel_listing, name='hotel'),
    url(r'^hotel_detail/(?P<id>\d+)/', views.hotel_detail, name='detail'),
     # or -- url(r'^$', post.views.post_home), if the views are  not impoted
     
]


