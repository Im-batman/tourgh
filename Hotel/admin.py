from django.contrib import admin
from .models import hotel
# Register your models here.




class hotelAdmin(admin.ModelAdmin):
    models= hotel
    max_num=2
    list_display=('name','region', 'town','rating',)
    list_filter = ['name', 'region']
    search_fields=['name',]




admin.site.register(hotel, hotelAdmin)