from rest_framework import serializers
from Data_Sorting.models import region,rating,town,site_kind
from .models import hotel
from Restaurant.models import restaurant
'''
class RegionSerializer(serializers.ModelSerializer):
	class Meta:
		model = region
		fields =('id', 'name')

class RatingSerializer(serializers.ModelSerializer):
	class Meta:
		model = region
		fields =('id','name')


class TownSerializer(serializers.ModelSerializer):
	class Meta:
		model = town
		fields = ('id','name')



class HotelSerializer(serializers.ModelSerializer):
	region= RegionSerializer()
	town=TownSerializer()
	rating=RegionSerializer()
	class Meta:
		model = hotel
		fields( 'id', 'region','rating','town', 'name', 'address','website','gmap_url',
			'email','phone','image','image2','image3','description')






'''

class RegionSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = region
		fields =('id', 'name')

class RatingSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = rating
		fields =('id','stars')


class TownSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = town
		fields = ('id','name')


class Site_kindSerializer(serializers.HyperlinkedModelSerializer):
	class Meta:
		model = site_kind
		fields = ('id','kind')


class HotelSerializer(serializers.HyperlinkedModelSerializer):
	region= RegionSerializer()
	town= TownSerializer()
	rating= RatingSerializer()

	class Meta:
		model = hotel
		fields =( 'id', 'region','rating','town', 'name','dollarprice','cediprice', 'address','website','gmap_url','lon','lat',
			'email','phone','image','image2','image3','description')

'''

class RestaurantSerializer(serializers.HyperlinkedModelSerializer):
	region= RegionSerializer()
	town= TownSerializer()
	rating= RatingSerializer()

	class Meta:
		model = restaurant
		fields = ields =( 'id', 'region','rating','town', 'name', 'address','website','gmap_url',
			'email','phone','image','image2','image3','description')

'''

