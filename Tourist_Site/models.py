from __future__ import unicode_literals

from django.db import models
from Data_Sorting.models import region,town, site_kind
# Create your models here.


class tourist_site(models.Model):
	name=models.CharField( max_length=100, unique=True)
	address=models.CharField(max_length=100)
	kind=models.ForeignKey(site_kind)
	region=models.ForeignKey(region)
	town=models.ForeignKey(town)
	lon=models.DecimalField(max_digits=8, decimal_places=6, null=True)
	lat=models.DecimalField(max_digits=8, decimal_places=6, null=True)
	gmap_url=models.CharField(max_length=200, null=True)
	email=models.EmailField(null=True)
	phone=models.CharField(max_length=10, blank=True)
	image=models.FileField(upload_to='site/', null=True, blank=True)
	image2=models.FileField(upload_to='site/', null=True, blank=True)
	image3=models.FileField(upload_to='site/', null=True, blank=True)
	website=models.URLField(blank=True)
	description=models.TextField()

	def __str__(self):
		return self.name
