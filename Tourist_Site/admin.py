from django.contrib import admin
from .models import tourist_site
# Register your models here.


class tourist_siteAdmin(admin.ModelAdmin):
    models= tourist_site
    max_num=2
    list_display=('name','kind','region', 'town',)
    list_filter = ['name', 'region']
    search_fields=['name',]



admin.site.register(tourist_site, tourist_siteAdmin)