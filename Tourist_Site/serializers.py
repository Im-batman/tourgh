from rest_framework import serializers
from Data_Sorting.models import region,rating,town
from .models import tourist_site
from Hotel.serializers import RegionSerializer, TownSerializer, Site_kindSerializer




class Tourist_siteSerializer(serializers.HyperlinkedModelSerializer):
	region= RegionSerializer()
	town= TownSerializer()
	kind= Site_kindSerializer()


	class Meta:
		model = tourist_site
		fields =( 'id', 'region','name','kind','town','address','website','gmap_url','lon','lat',
			'email','phone','image','image2','image3','description')


