from __future__ import unicode_literals

from django.apps import AppConfig


class TouristSiteConfig(AppConfig):
    name = 'Tourist_Site'
