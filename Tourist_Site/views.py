from django.shortcuts import render

# Create your views here.



from rest_framework import viewsets
from .models import tourist_site
from Data_Sorting.models import region,rating,town
#from Restaurant.models import restaurant
from serializers import  Tourist_siteSerializer
import django_filters
	
# Create your views here.
class Tourist_siteViewSet(viewsets.ModelViewSet):
 	queryset = tourist_site.objects.all()
 	serializer_class = Tourist_siteSerializer
 	filter_backends = (django_filters.rest_framework.DjangoFilterBackend,)
 	filter_fields = ('region', 'kind','town','name')


